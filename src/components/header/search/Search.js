import React, { Component } from 'react'

import { ReactComponent as ArrowLeftIcon } from 'icons/arrow-left.svg'
import { ReactComponent as SearchIcon } from 'icons/search.svg'

import './Search.scss'

class Search extends Component {
	state = {
		value: '',
		expanded: false,
		isLabelUp: false,
	}

	handleValueChange = ({ target: { value } }) => {
		this.setState(prevState => ({
			value,
			isLabelUp: !!value || prevState.isLabelUp
		}))
	}

	handleToggle = () => {
		this.setState(prevState => ({ expanded: !prevState.expanded }))
	}

	handleLiftLabel = isLabelUp => () => {
		this.setState({ isLabelUp })
	}

	render() {
		const { value, expanded, isLabelUp } = this.state

		return (
			<label
				className={expanded ? 'search search--expanded' : 'search'}
				onFocus={this.handleLiftLabel(true)}
				onBlur={this.handleLiftLabel(!!value)}
			>
				<div className="header_action" onClick={this.handleToggle}>
					{expanded ? <ArrowLeftIcon /> : <SearchIcon />}
				</div>

				<input
					className="input"
					value={value}
					onChange={this.handleValueChange}
					type="search"
				/>

				<span className={isLabelUp ? 'label label--up' : 'label'}>
					Search
				</span>
			</label>
		)
	}
}

export default Search
