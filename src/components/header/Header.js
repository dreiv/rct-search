import React from 'react'

import { ReactComponent as BellIcon } from 'icons/bell.svg'
import { ReactComponent as MenuIcon } from 'icons/menu.svg'
import { ReactComponent as UserIcon } from 'icons/user.svg'

import { Search } from './search'

import './Header.scss'

const Header = () => (
	<header className="header">
		<div className="header_wrapper">
			<MenuIcon /> Menu
			<Search />
			<button className="header_action">
				<BellIcon />
			</button>
			<button className="header_action user">
				<UserIcon />
			</button>
		</div>
	</header>
)

export default Header
